# Preparation

## Description
This repository contains the codes which create the database used to predict the presence of Xylella fastidiosa. 
The steps to follow to retreive the dataset are described below.

## How to reproduce the code

### 1) Clima
The first code to run is the "creation of BIO variables.R" which re-create the BIO variables of WordClim using data from Safran. Then it is merged with the safran data. 
Secondly, run the "voronoi clima.R" code, which interpolates the values until the coast where the grid of safran was not going. 

### 2) Manganese Cuivre Zinc
The code "voronoi mn zc cu.R" is the same as "voronoi clima.R", it interpolates the data to the all region.

### 3) Aspect Map
Run "aspect crop chunk.R" which transforms the data which are integer from 0 to 255 to cardinal and inter-cardinal points (i.e "N", "NO", "O" etc) and compute the proportion of values of observations which fall whithin a cell of the grid of Xylella.

### 4) Corine Land Cover
Run "corine croptest.R" which compute the proportion of values of observations which fall whithin a cell of the grid of Xylella.

### 5) Relief
"Aggregate relief" is a code to aggregate the 10 tif files taken from the Shuttle Radar Topography Mission that I dowload from "http://dwtkns.com/srtm/".
Run "relief mean.R" which compute the mean value of observations which fall whithin a cell of the grid of Xylella.

### 6) Merge the output of the previous steps
The code "merge all output.R" merges the output into on single spatial dataframe at the xylella grid level. It also deletes the useless variables.

### 7) Cleaning 
The code "cleaning imputation.R" removes the variables with more than 20 % of missing values. Correct the names of some variables and remove useless variables. It adds the department number for the few cells that did not have it. It also add the variable *sample* which tell whether a cell has been sampled at least once. Finally, it interpolates all missing values ( which corresponds to less than 20% of the data for each variable).


The code "change name variables.R" only changes the name of some variables and remove some redundant variables.


